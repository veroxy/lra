# LRA PROJECT - La racine Acoustik 2020
 
# [Start Bootstrap - Creative](https://startbootstrap.com/template-overviews/creative/)

[Creative](http://startbootstrap.com/template-overviews/creative/) is a one page creative theme for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/).

## Preview

[![Creative Preview](https://bitbucket.org/veroxy/lra/src/master/preview_lra.png)](https://bitbucket.org/veroxy/lra)

**[View Live Preview](https://blackrockdigital.github.io/startbootstrap-creative/)**

## Status

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/BlackrockDigital/startbootstrap-creative/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/startbootstrap-creative.svg)](https://www.npmjs.com/package/startbootstrap-creative)
[![Build Status](https://travis-ci.org/BlackrockDigital/startbootstrap-creative.svg?branch=master)](https://travis-ci.org/BlackrockDigital/startbootstrap-creative)
[![dependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-creative/status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-creative)
[![devDependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-creative/dev-status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-creative?type=dev)

## Download and Installation

To begin using this template, choose one of the following options to get started:
* [Download the latest release on Start Bootstrap](https://startbootstrap.com/template-overviews/creative/)
* Install via npm: `npm i startbootstrap-creative`
* Clone the repo: `git clone https://github.com/BlackrockDigital/startbootstrap-creative.git`
* [Fork, Clone, or Download on GitHub](https://github.com/BlackrockDigital/startbootstrap-creative)



## Nommage fichier
- Toute les fichiers médias sont préfixé par "**lra_**" et suivis de son nom, sans accent sans espace sans caractère spéciaux sauf desz tiret de liaison quand c'est un mot composé. Les espace sont remplacées par "**_**". Ce n'est pas pas par soucis d'ethetisme c'est une norme informatique pour la sensibilité à la case. Puis que le document ne sera pas lisible!!
> "fiche scénique.pdf" =  lra_fiche_scenique.pdf

## ficher xls.


## Copyright and License
Copyright 2013-2019 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-creative/blob/gh-pages/LICENSE) license.
