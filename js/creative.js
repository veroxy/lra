(function ($) {
    "use strict"; // Start of use strict

    /**
     *
     * @param el
     * @returns {any}
     */
    $.create = function(el){
        return document.createElement(el);
    };

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        var pathName            = location.pathname;
        var pathNameReplaceable = pathName.replace(/^\//, '');

        if (pathNameReplaceable == this.pathname.replace(/^\//, '') && pathName == this.hostname) {
            var target = $(this.hash);
            target     = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 72)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 75
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-scrolled");
        } else {
            $("#mainNav").removeClass("navbar-scrolled");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

    // Magnific popup calls
    $('#gallery').magnificPopup({
        delegate: '.gallery-box',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });

})(jQuery); // End of use strict

//https://demos.creative-tim.com/rubik/startup.html
var lra = {
    showModal: function (button) {
        var id       = $(button).data('target');
        var $project = $(button).closest('.project');

        var scrollTop   = $(window).scrollTop();
        var distanceTop = $project.offset().top;

        var projectTop    = distanceTop - scrollTop;
        var projectLeft   = $project.offset().left;
        var projectHeight = $project.innerHeight();
        var projectWidth  = $project.innerWidth();

        modal = $('#' + id);

        $(modal).css({
            'top': projectTop,
            'left': projectLeft,
            'width': projectWidth,
            'height': projectHeight,
            'z-index': '1032'
        });

        $(modal).addClass('has-background');

        setTimeout(function () {
            $(modal).addClass('open');
        }, 30);

        setTimeout(function () {
            $('body').addClass('noscroll');
            $(modal).addClass('scroll');
        }, 1000);

        $('.icon-close').click(function () {
            $project_content = $(this).closest('.project-content');
            $project_content.removeClass('open scroll');

            $('body').removeClass("noscroll");
            //$('a').removeClass('no-opacity');
            setTimeout(function () {
                $project_content.removeClass('has-background');
                setTimeout(function () {
                    $project_content.removeAttr('style');
                }, 450);
            }, 500);
        });
    },

    /*****************************************************************************
     * GOOGLE MAPS
     ****************************************************************************/
    initGoogleMaps: function () {
        var myLatlng = new google.maps.LatLng(44.433530, 26.093928);

        var mapOptions = {
            zoom: 16,
            center: myLatlng,
            scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
            disableDefaultUI: true,
            styles: [{
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [{"visibility": "on"}, {"gamma": "1.82"}]
            }, {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{"visibility": "on"}, {"gamma": "1.96"}, {"lightness": "-9"}]
            }, {
                "featureType": "administrative",
                "elementType": "labels.text.stroke",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{"visibility": "on"}, {"lightness": "25"}, {"gamma": "1.00"}, {"saturation": "-100"}]
            }, {
                "featureType": "poi.business",
                "elementType": "all",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "poi.park",
                "elementType": "all",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{"hue": "#ffaa00"}, {"saturation": "-43"}, {"visibility": "on"}]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road.highway",
                "elementType": "labels",
                "stylers": [{"visibility": "simplified"}, {"hue": "#ffaa00"}, {"saturation": "-70"}]
            }, {
                "featureType": "road.highway.controlled_access",
                "elementType": "labels",
                "stylers": [{"visibility": "on"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers": [{"visibility": "on"}, {"saturation": "-100"}, {"lightness": "30"}]
            }, {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [{"saturation": "-100"}, {"lightness": "40"}, {"visibility": "off"}]
            }, {
                "featureType": "transit.station.airport",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"gamma": "0.80"}]
            }, {"featureType": "water", "elementType": "all", "stylers": [{"visibility": "off"}]}]
        };
        var map        = new google.maps.Map(document.getElementById("contactUsMap"), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            title: "Hello World!"
        });
        // To add the marker to the map, call setMap();
        marker.setMap(map);
    },

    scrollPage: function () {
        /*
     Back Top Link
     ========================================================================== */
        var offset   = 200;
        var duration = 500;
        $(window).scroll(function () {
            if ($(this).scrollTop() > offset) {
                $('#player-soundcloud').fadeIn(400);
                console.log(offset)
            } else {
                $('#player-soundcloud').fadeOut(400);
                console.log("!=< " + offset)
            }
        });

        // $('#player-soundcloud').on('click',function(event) {
        //     event.preventDefault();
        //     $('html, body').animate({
        //         scrollTop: 0
        //     }, 600);
        //     return false;
        // });  
    },


    /** TOFIXED METTRE A LA NORM ICI LES AUTRES API **/

    onYouTubePlayerAPIReady :  function () {
        var player = new YT.Player('video_name_date_id', {
            height: '360',
            width: '900px',
            videoId: 'EfIxgPwJCvM'
        });
    }
};

/*****************************************************************************
 * TRACKER RGPD
 ****************************************************************************/
tarteaucitron.init({
    "privacyUrl": "", /* Privacy policy url */

    "hashtag": "#tarteaucitron", /* Open the panel with this hashtag */
    "cookieName": "tarteaucitron", /* Cookie name */

    "orientation": "middle", /* Banner position (top - bottom) */
    "showAlertSmall": true, /* Show the small banner on bottom right */
    "cookieslist": true, /* Show the cookie list */

    "adblocker": false, /* Show a Warning if an adblocker is detected */
    "AcceptAllCta": true, /* Show the accept all button when highPrivacy on */
    "highPrivacy": true, /* Disable auto consent */
    "handleBrowserDNTRequest": false, /* If Do Not Track == 1, disallow all */

    "removeCredit": false, /* Remove credit link */
    "moreInfoLink": true, /* Show more info link */
    "useExternalCss": false, /* If false, the tarteaucitron.css file will be loaded */

    //"cookieDomain": ".my-multisite-domaine.fr", /* Shared cookie for subdomain website */

    "readmoreLink": "/cookiespolicy" /* Change the default readmore link pointing to opt-out.ferank.eu */
});



/*****************************************************************************
 *SOUNDCLOUD
 ****************************************************************************/
SC.initialize({
    client_id: 'YOUR_CLIENT_ID'
});

var track_url = 'https://soundcloud.com/forss/flickermood';
SC.oEmbed(track_url, {auto_play: true}).then(function (oEmbed) {
    console.log('oEmbed response: ', oEmbed);
});

/*****************************************************************************
 *YOUTUBE
 ****************************************************************************/
    // Load the IFrame Player API code asynchronously.
var tag            = $.create('script');
tag.src            = "https://www.youtube.com/player_api";

var firstScriptTag = $('script')[0];

firstScriptTag.parent().insertBefore(tag, firstScriptTag);

// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;
// init player HERE TODO

